...

# Processes

---

## How decision are made in amorphous groups

> from the Greek a, without, morphé, shape, form

> having no definite form, SHAPELESS; 

> lacking organization or unity

<div style="text-align: center;"> 
	<img src="img/amorphous.png" style="width: 100%;" />
</div>

???

> Whether a material is liquid or solid depends primarily on the connectivity between its elementary building blocks so that solids are characterized by a high degree of connectivity whereas structural blocks in fluids have lower connectivity.

---

layout: false
background-image: url(img/cynefinM.png)
background-size: contain
background-repeat: no-repeat
background-position: center middle

???

## Decision making

When we are in Complex or Chaotic domain (see: Cynefin framework by professor Dave Snowden) but we cannot agree on how to act/probe?

How decisions are made where to put effort with limited resources/time?


How decisions are made where to put effort with limited resources/time?

---

layout: false
background-image: url(img/bazaar2.png)
background-size: contain
background-repeat: no-repeat
background-position: center right

.left-column[
> No quiet, reverent cathedral-building here—rather, the Linux community seemed to resemble a great babbling bazaar of differing agendas and approaches [...] out of which a coherent and stable system could seemingly emerge only by a succession of miracles. 

> The fact that this bazaar style seemed to work, and work well, came as a distinct shock.
]
.right-column[
  
]

---

layout: false
background-image: url(img/mythical.jpg)
background-size: contain
background-repeat: no-repeat
background-position: center right

.left-column[

# Too many communications?

> [Fred Brooks in Mythical Man-Month] argued that the complexity and communication costs of a project rise with the square of the number of developers, while work done only rises linearly [...] if Brooks’s Law were the whole picture Linux would be impossible

]

---

# When everyone wants the same, minus ego

> In his discussion of “egoless programming”, [Gerald] Weinberg observed that in shops where developers are not territorial about their code, and encourage other people to look for bugs and potential improvements in it, improvement happens dramatically faster than elsewhere.

--

<div style="text-align: center;"> 
	<img src="img/krwa.jpg" style="width: 100%;" />
</div>

???

But we are territorial: my microservices (product) are not yours!

At the same time, whole system architecture requires cooperation, egoless approach

I have yet to see a corporation without internal politics being played out

> But what is this leadership style and what are these customs? They cannot be based on power relationships

---

# Principle of shared understanding


> [...] connect the selfishness of individual hackers as firmly as possible to difficult ends that can only be achieved by sustained cooperation. 

> [..] the "principle of command" is effectively impossible to apply among volunteers in the anarchist's paradise we call the Internet. To operate and compete effectively, hackers who want to lead collaborative projects have to learn how to recruit and energize effective communities of interest in the mode vaguely suggested by Kropotkin's "principle of understanding"

> Linus, by successfully positioning himself as the gatekeeper of a project in which the development is mostly done by others, and nurturing interest in the project until it became self-sustaining, has shown an acute grasp of Kropotkin’s “principle of shared understanding”

---

# Self motivation

> The success of the open-source community sharpens this question considerably, by providing hard evidence that it is often cheaper and more effective to recruit self-selected volunteers from the Internet than it is to manage buildings full of people who would rather be doing something else.

<div style="text-align: center;"> 
	<img src="img/motivation2.gif" style="width: 100%;" />
</div>

---

class: center, inverse
layout: false
background-image: url(img/team.png)
background-size: contain
background-repeat: no-repeat
background-position: center right

# Treat people like your peers

???

My volleyball coach would adress 11y old: "Gentlemen, you are too good to play like this"

Empowerment & Trust

When you treat people as kids, they behave like kids

Help people grow, and they will love you, and they will feel motivated

Treat them like the best, and they will strive to be meet that expectations


---

# Boring things that need to be done

> Many people (especially those who politically distrust free markets) would expect a culture of self-directed egoists to be fragmented, territorial, wasteful, secretive, and hostile. 

> But this expectation is clearly falsified by (to give just one example) the stunning variety, quality, and depth of Linux documentation. It is a hallowed given that programmers hate documenting; how is it, then, that Linux hackers generate so much documentation? Evidently Linux’s free market in egoboo works better to produce virtuous, other-directed behavior than the massively-funded documentation shops of commercial software producers.

---

# The role of an architect is helping others

> [...] it’s doubly important that open-source hackers organize themselves for maximum productivity by self-selection—and the social milieu selects ruthlessly for competence. My friend, familiar with both the open-source world and large closed projects, believes that open source has been successful partly because its culture only accepts the most talented 5% or so of the programming population. 

> She spends most of her time organizing the deployment of the other 95%

--

The rock stars can handle themselves. The goal of an architect in a microservice environment is to help the rest 95%. Make their job easy and painless. 

---

# Communication skills

> There is another kind of skill not normally associated with software development which I think is as important as design cleverness to bazaar projects—and it may be more important. A bazaar project coordinator or leader must have good people and communications skills.

We need communication skills to teach (bring others up to speed), share knowledge (principle of common understanding) and finally it is what moves ones agenda forward.

How to improve communication skills?

---

## The nerd-introvert syndrome

Q - How can you tell if a programmer has high social skills

A - During conversation he stares at YOUR shoes

<div style="text-align: center;"> 
	<img src="img/lisp.png" style="width: 50%;" />
</div>

---

## Introvert vs Extravert

 > Extraversion tends to be manifested in outgoing, talkative, energetic behavior, whereas introversion is manifested in more reserved and solitary behavior

> Extraversion is the state of primarily obtaining gratification from outside oneself. Extraverts are energized and thrive off being around other people.

> [Introverts] people whose energy tends to expand through reflection and dwindle during interaction.

--

> Extraversion and introversion are typically viewed as a single continuum, so to be high in one necessitates being low in the other. Jung provides a different perspective and suggests that everyone has both an extraverted side and an introverted side, with one being more dominant than the other. 

---

layout: false
background-image: url(img/sisters2.jpg)
background-size: contain
background-repeat: no-repeat
background-position: center middle

???

My sisters would spend 14h talking with and being around people

I'd spend 10h looking at a computer screen or reading books

That's 10h/d difference in practicing human communication

My sisters love watching drama, social or romance movies

I love sci-fi, action, thriller. I hate watching people interact in tough social context

---

layout: false
background-image: url(img/sisters.jpg)
background-size: contain
background-repeat: no-repeat
background-position: center middle

## It's a matter of practice

???

My sisters used to be much better in social context than me event though I was 5/10 years older.

With time that difference dissapears


---

## Monitoring of the system

Your brain is a parallel processor (unless it is actually a memory system mainly, vide "On Intelligence" by Jeff Hawkins & Sandra Blakeslee).

You can start a monitoring thread. 

It's actually what scientists, religion gurus and poets suggest.


<div style="text-align: center;"> 
	<img src="img/artofbeing.jpg" style="width: 30%;" />
	<img src="img/awarness.jpg" style="width: 30%;" />
	<img src="img/fabula.jpg" style="width: 30%;" />	
</div>

???

Self analysis: "The Art of Being" by Erich Fromm

"Awareness: The Perils and Opportunities of Reality" Anthony De Mello

"Fabula Rasa" by  Edward Stachura


---

layout: false
background-image: url(img/bridge.jpeg)
background-size: contain
background-repeat: no-repeat
background-position: center right

.left-column[

## Monitor emotions

Look for your emotions

"Some evidence for heightened sexual attraction under conditions of high anxiety." Dutton & Aron, 1974

]

???

> 85 male passersby were contacted either on a fear-arousing suspension bridge or a non-fear-arousing bridge by an attractive female interviewer who asked them to fill out questionnaires containing Thematic Apperception Test (TAT) pictures. Sexual content of stories written by Ss on the fear-arousing bridge and tendency of these Ss to attempt postexperimental contact with the interviewer were both significantly greater. No significant differences between bridges were obtained on either measure for Ss contacted by a male interviewer.

---

layout: false
background-image: url(img/wojciszke.jpeg)
background-size: 40%
background-repeat: no-repeat
background-position: center right

.left-column[

##  Brain fails to understand the source of emotions

]

???

namiętność może być częściowo, anawet wyłącznie, wynikiem fałszywego uświadomienia sobie przez nas naszego pobudzenia jako wywołanego przez obiekt naszej namiętności, podczas gdy w rzeczywistości zostało ono wywołane czymś zupełnie innym.

Fascynujące jest to, iż rzeczywiste źródło pobudzenia nie musi być wcale przyjemne, może być także neutralne, a nawet nieprzyjemne, jak to było w przypadku opisanego eksperymentu z mostami.


---

## How does monitoring work

You realize you feel anger or annoyance

You realize it has nothing to do with the subject of the problem

You do not let it control your behavior, you decide what to do based on intelect instead of emotions

---

## Emotions

You have no control on emerging emotions

You either let them overwhelm you (decide on your actions, strengthen them)

Or you decided to act despite your emotions 

(it's not about blocking your emotions, it's about who controls actions of the body: conscious mind or emotions)

---

## Monitor 2.0

First you do probing on "hey monitor, how do I feel and why"?

Then you put it on autopilot "inform me on high emotions and source". Your brain does it for you without your consciousness.

Just like driving it requires time to internalize.

Remember, you need to check the monitoring system from time to time, or you'll not know when it stops working.

Also, look for logical fallacies in your own words to find hidden emotions and biases

> A fallacy (also called sophism) is the use of invalid or otherwise faulty reasoning, or "wrong moves" in the construction of an argument.

---

## Monitor 3.0

Now start monitoring the energy level of the discussion and other participants as well using empathy

When you see energy levels too low or too high, change strategy

Too low - make space for the quiet types to express themselves, ask open questions more often, ask for their insight, make environment safe

Too high - slow down, calm down yourself, find the root couse of emotions on the other side (often it is fear)

---

## Sympathy

> Sympathy is the perception, understanding, and reaction to the distress or need of another life form.

Our emotions and behaviours happen often DESPITE our will

If you want to understand why people behave the way they do, and get some sympathy for the failing human, read:

"Behave. The Biology of Humans at Our Best and Worst" by professor Robert M. Sapolsky

or watch recordings of his "Human Behavioral Biology" lectures at Stanford

https://www.youtube.com/watch?v=NNnIGh9g6fA

> Thus, it is impossible to conclude that a behavior is caused by a gene, a hormone, a childhood trauma, because the second you invoke one type of explanation, you are de facto invoking them all. No buckets. A “neurobiological” or “genetic” or “developmental” explanation for a behavior is just shorthand, an expository convenience for temporarily approaching the whole multifactorial arc from a particular perspective.

---

layout: false
background-image: url(img/nonviolent.jpeg)
background-size: 100%
background-repeat: no-repeat
background-position: center middle

???

## Corrective steps after monitoring

### Nonviolent Communication

> Consciousness - A set of principles that support living a life of compassion, collaboration, courage, and authenticity.

> Language - Understanding how words contribute to connection or distance.

> Communication - Knowing how to ask for what you want, how to hear others even in disagreement, and how to move forward towards solutions that work for all.

> Means of influence - Sharing “power with others” rather than using “power over others”.

Porozumienie bez przemocy

---

class: center

### Provide emotional support 

<iframe width="100%" height="80%" src="https://www.youtube.com/embed/vmsPX-b4lS4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

layout: false
background-image: url(img/puchargrodzisk.jpg)
background-size: contain
background-repeat: no-repeat
background-position: center middle

---

> Dzięki Kuba, w pewnym momencie czułem że może się nie dogadamy, a jakoś Twój wkład to trochę załagodził. Ja miałem trochę cykora bo byłem świadom tych dokumentów a trochę nie podeszłem do tematu rozdzielając proda od reszty. Ogólnie na początku narzekałem że wszystkim działa a wam jako A-Team nie, ale widzę że to właśnie wasz wkład w to wszystko może dać najlepszy efekt

???

Context - my team is very unhappy due to infra k8s and security changes, which make our work difficult. We get a video meeting with "The single person to blame" for all the changes. Anger and frustration in the air. Blood could be drawn.

I start it soft, with focus on what each side really needs and why. I voice my lack of competence (things I don't know about) and ask the other side for helping me understand.

---

> (a situtional joke being told, finishing the meeting)

> me: "C U later. I love you guys!"

> You have such good humor! Thanks for all your support on the architecture topics that we have tackles up till now!

???

Context - a multi country architecture video meeting, crossing cultures, 30+ years age difference, no shared language, some poeple have never seen each other in real life, others have a lot in common.


---

class: inverse
layout: false
background-image: url(img/zosia.jpg)
background-size: contain
background-repeat: no-repeat
background-position: center right

# Make people grow

Your goal is not to be the best

Your goal is to help people be better

Move out of the spotlight

---

layout: false
background-image: url(img/Onwriting.jpg)
background-size: contain
background-repeat: no-repeat
background-position: center right

# Learn to write

Go to a creative writing course

Read books about writing

---

layout: false
background-image: url(img/aubrey-plaza.jpg)
background-size: contain
background-size: 75% auto
background-repeat: no-repeat
background-position: center bottom 

# Charm

> "It is not a coincidence that Linus is a nice guy who makes people like him and want to help him. It’s not a coincidence that I’m an energetic extrovert who enjoys working a crowd and has some of the delivery and instincts of a stand-up comic. To make the bazaar model work, it helps enormously if you have at least a little skill at charming people."


--

> "Charisma, a person or thing's pronounced ability to attract others"

---

# Charisma

>  "a type of leadership whose nature is based on values (i.e., morals), beliefs and symbolism as well as on emotion, which is expressive in its transmission of information."

> "the discretion and the means to asymmetrically enforce one’s will"

> "Important for Weber was the notion that the very goals of a charismatic leader are different to those of institutional methods of influence. Weber viewed charisma as radical force wherein “charismatic domination is the very opposite of bureaucratic domination,” working against methodical, rational, and economic ideals (Weber 1968, p. 20)."

> "we think that the term “signaling” as a general mechanism of information communication should also appear in a definition of charismatic leadership. Note that signaling canoccur via verbal and nonverbal communication modes (Awamleh & Gardner 1999, Frese et al 2003, Towler 2003, Willner 1984); and, interestingly, those who are high on use of verbal communication means are high on nonverbal too (Antonakis et al 2011) presumably because rich verbal communication means (e.g., storytelling) require more nonverbal gesturing (Jacquart & Antonakis 2015, Towler 2003)."

> [Charisma definition] "Charisma is a values-based, symbolic, and emotion-laden leader signaling. "

> "Theoretically, the “connection” (i.e., the charismatic effect) that a charismatic leader has with his or her followers, and why the followers identify with the leader, stems from the leader (a) justifying the mission by appealing to values that distinguish right from wrong, (b) communicating in symbolic ways to make the message clear and vivid, and also symbolizing and embodying the moral unity of the collective per se, and (c) demonstrating conviction and passion for the mission via emotional displays (Antonakis et al 2011)."

> "indeed, charismatic leaders can be loved but also much loathed by those who do not share in the leader’s values (Tucker 1968)"

> "Also, the leader must appropriately communicate about actions in which the collective should invest; leaders do so via beliefs and expectations and this using symbolic communication means (e.g., metaphors) and displays of emotions in a correctly-calibrated and appropriate manner."

> "Summary points: [...] Charisma can be developed."

> ["Charisma: An Ill-Defined and Ill-Measured Gift", Antonakis, John; Bastardoz, Nicolas; Jacquart, Philippe; Shamir, Boas (2016)](https://serval.unil.ch/resource/serval:BIB_43EBA02BAD5E.P001/REF)

--

"When Does Charisma Matter for Top-Level Leaders? Effect of Attributional Ambiguity", Jacquart, Philippe; Antonakis, John (2015)

"The Charisma Myth: How Anyone Can Master the Art and Science of Personal Magnetism" Olivia Fox Cabane (2013)

Study yourself, study others

---

layout: false
background-image: url(img/gosling-pitt.jpeg)
background-size: contain
background-size: 100% auto
background-repeat: no-repeat

### Kids can do it, so can you

---

# You can master communication

In fact, you have your whole life to do it

Thanks!

